import React from "react";
import "../assets/css/Asignacion.css";
import { Form, Col, Row, Button } from "react-bootstrap";
import {
  BootstrapTable,
  TableHeaderColumn,
  ExportCSVButton,
} from "react-bootstrap-table";
import { DragDropContext } from "react-beautiful-dnd";
import { BsSearch, BsFillPersonCheckFill } from "react-icons/bs";
import Column from "./Drag/Column";
import { URL_SERVICES } from "../constants/contants.js";
import NavBar from "../components/NavBar";

class Asignacion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show_btn_assing: false,
      describe: "Buscar",
      usersList: [], // Lista de todos los empleados a cargo del Usuario
      userListSelect: 0, // Valor por default del select del area de busqueda
      projectSelect: 0, // Valor por default del select de projectos

      projectSelectEstado: 0, // Valor por default del select de projectos
      projectSelectLocalidad: 0, // Valor por default del select de projectos

      projectsList: [], // Lista de proyectos
      inventarioList: [], // Lista de inventarios
      estadosList: [], // Lista de estados
      localidadesList: [], // Lista de localidades

      employeeSelected: [],
      numIterator: 0,

      // Option to Table of Employs
      options: {
        sizePerPageList: [
          {
            text: "5",
            value: 5,
          },
          {
            text: "10",
            value: 10,
          },
          {
            text: "15",
            value: 15,
            //text: 'All', value: inventariosid.length
          },
        ], // you can change the dropdown list for size per page
        sizePerPage: 5, // which size per page you want to locate as default
        paginationPosition: "top", // default is bottom, top and both is all available
        noDataText: "Sin registros",
      },
      selectRow: {
        mode: "checkbox",
        bgColor: "",
        className: "my-selection-custom",
      },
    };

    console.log(this.state);
    this.getListAdminUsers();
  }

  /**
   * Get List of Users to show on Search select options
   */
  getListAdminUsers() {
    let url = URL_SERVICES + `users`;

    let headers = { "Content-type": "application/json;" };

    // Request options
    let options = {
      method: "GET",
      mode: "cors",
      cache: "default",
      header: headers,
    };

    fetch(url, options)
      .then((response) => response.json())
      .then((usersList) => {
        console.log(usersList);
        this.setState({ usersList: usersList });
      })
      .catch((err) => console.log(err));
  }

  /**
   * Get Employs of selected User on Search List
   */
  getUsersEmployees(id) {
    let url = URL_SERVICES + `users/employees/${id}`;

    let headers = { "Content-type": "application/json;" };

    // Request options
    let options = {
      method: "GET",
      mode: "cors",
      cache: "default",
      header: headers,
    };

    fetch(url, options)
      .then((response) => response.json())
      .then((usersList) => {
        console.log(usersList);

        this.setState({ employsUserSelected: usersList });
      })
      .catch((err) => console.log(err));
  }

  /**
   * Get List of Projects
   */
  getListProjects() {
    let url = URL_SERVICES + `inventario/projectsid`;

    let headers = { "Content-type": "application/json;" };

    // Request options
    let options = {
      method: "GET",
      mode: "cors",
      cache: "default",
      header: headers,
    };

    fetch(url, options)
      .then((response) => response.json())
      .then((projectsList) => {
        console.log(projectsList);
        this.setState({ projectsList: projectsList });
      })
      .catch((err) => console.log(err));
  }

  /**
   * Obtiene la Lista del Inventario por proyecto
   */
  getListInventario(projectid, usuarioid) {
    console.log("getListInventario :: Proyecto id: " + projectid);
    let url = URL_SERVICES + `inventario/projects/noasign/${projectid}`;
    let headers = { "Content-type": "application/json;" };

    // Request options
    let options = {
      method: "GET",
      mode: "cors",
      cache: "default",
    };

    fetch(url, options)
      .then((response) => response.json())
      .then((projectsInv) => {
        console.log(projectsInv);
        this.setState({ inventarioList: projectsInv });
      })
      .catch((err) => console.log(err));
  }

  /**
   * Obtiene la Lista de los Estados del Inventario del proyecto Selecccionado
   */
  getInvEstados(id) {
    console.log("getInvEstados :: Proyecto id: " + id);
    let url = URL_SERVICES + `inventario/projects/estado/${id}`;

    let headers = { "Content-type": "application/json;" };

    // Request options
    let options = {
      method: "GET",
      mode: "cors",
      cache: "default",
    };

    fetch(url, options)
      .then((response) => response.json())
      .then((estadoInv) => {
        console.log(estadoInv);
        this.setState({ estadosList: estadoInv });
      })
      .catch((err) => console.log(err));
  }

  /**
   * Obtiene la Lista de las localidades de los Estados del Inventario del proyecto Selecccionado
   */
  getInvLocalidades(idproyecto, estado) {
    let url = URL_SERVICES + `inventario/projects/localidades`;

    const dataCampos = new FormData();
    dataCampos.append("proyectoid", idproyecto);
    dataCampos.append("estado", estado);

    let headers = { "Content-type": "application/json;" };

    // Request options
    let options = {
      method: "POST",
      mode: "cors",
      cache: "default",
      body: dataCampos,
    };

    fetch(url, options)
      .then((response) => response.json())
      .then((localidades) => {
        console.log(localidades);
        this.setState({ localidadesList: localidades });
      })
      .catch((err) => console.log(err));
  }

  /**
   * Get List of Inventario Asignado al Usuario
   */
  getListInventarioUsuarios(idusuario) {
    let url = URL_SERVICES + `inventario/projects/user/${idusuario}`;

    let headers = { "Content-type": "application/json;" };

    // Request options
    let options = {
      method: "GET",
      mode: "cors",
      cache: "default",
      header: headers,
    };

    fetch(url, options)
      .then((response) => response.json())
      .then((invuser) => {
        console.log(invuser);
        this.setState({ inventarioList: invuser });
      })
      .catch((err) => console.log(err));
  }

  /**
   * Select a User
   */
  selectUser = (e) => {
    this.setState({
      userListSelect: e.target.value,
      projectSelect: 0,
      projectSelectEstado: "",
      projectSelectLocalidad: "",
      projectsList: [],
      estadosList: [],
      localidadesList: [],
      inventarioList: [],
    });

    this.refs.refEstado.cleanFiltered();
    this.refs.refLocalidad.cleanFiltered();

    if (e.target.value > 0) {
      console.log(this.state.show_btn_assing);

      if (this.state.show_btn_assing) {
        this.getListInventarioUsuarios(e.target.value);
      } else {
        this.getListProjects();
      }
    } else {
    }
  };

  /**
   * List of projects :: Select project onChancge
   * @param {*} e
   */
  selectProject = (e) => {
    e.preventDefault();

    this.setState({
      projectSelect: e.target.value,
      projectSelectEstado: "",
      estadosList: [],
      projectSelectLocalidad: "",
      localidadesList: [],
    });

    this.refs.tableinv.cleanSelected();
    this.refs.refEstado.cleanFiltered();
    this.refs.refLocalidad.cleanFiltered();

    if (e.target.value > 0) {
      console.log("Proyecto id: " + this.state.projectSelect);
      console.log("Proyecto id: " + e.target.value);
      console.log("Usuaeio ID: " + this.state.userListSelect);

      this.getListInventario(e.target.value, this.state.userListSelect);
      this.getInvEstados(e.target.value);
    } else {
      this.setState({
        projectSelectEstado: "",
        estadosList: [],
        inventarioList: [],
      });
    }
  };

  /**
   *
   * @param {*} e
   */
  selectProjectEstado = (e) => {
    e.preventDefault();

    this.setState({
      projectSelectEstado: e.target.value,
      projectSelectLocalidad: "",
      localidadesList: [],
    });
    this.refs.tableinv.cleanSelected();
    this.refs.refLocalidad.cleanFiltered();

    if (e.target.value !== "") {
      console.log("Project id: " + this.state.projectSelect);
      console.log("Estado: " + e.target.value);

      this.refs.refEstado.applyFilter(e.target.value);

      this.getInvLocalidades(this.state.projectSelect, e.target.value);
    } else {
      this.refs.refEstado.cleanFiltered();
    }
  };

  /**
   *
   * @param {*} e
   */
  selectProjectLocalidad = (e) => {
    e.preventDefault();

    this.setState({ projectSelectLocalidad: e.target.value });

    this.refs.tableinv.cleanSelected();
    this.refs.refLocalidad.applyFilter(e.target.value);

    if (e.target.value == "") {
      this.refs.refLocalidad.cleanFiltered();
    }
  };

  asignInventario = (e) => {
    e.preventDefault();
    console.log("Click add inventario");
    console.log(this.refs.tableinv.store);
    console.log(this.refs.tableinv.store.selected);
    console.log(this.refs.tableinv.store.filteredData);

    let url = URL_SERVICES + `inventario_usuarios`;
    var data =
      this.refs.tableinv.store.selected.length > 0
        ? this.refs.tableinv.store.selected
        : this.refs.tableinv.store.filteredData
        ? this.refs.tableinv.store.filteredData
        : this.state.inventarioList;

    console.log(data);

    if (this.state.userListSelect > 0) {
      if (this.state.projectSelect > 0) {
        for (var i = 0; i < data.length; i++) {
          var item = data[i];
          var invid =
            this.refs.tableinv.store.selected.length > 0
              ? data[i]
              : item.inventarioid;

          console.log(item);
          console.log(this.state.userListSelect);
          console.log(invid);

          const dataCampos = new FormData();
          dataCampos.append("usuarioid", this.state.userListSelect);
          dataCampos.append("inventarioid", invid);

          let options = {
            method: "POST",
            body: dataCampos,
            mode: "cors",
            cache: "default",
          };

          fetch(url, options)
            .then((response) => response.text())
            .then((texto) => {
              console.log("Datos :: Asignación Inventario");
              console.log(texto);
            })
            .catch((err) => console.log(err));

          if (i + 1 === data.length) {
            this.setState({
              inventarioList: [],
            });

            this.setState({
              userListSelect: 0,
              projectSelect: 0,
              projectSelectEstado: "",
              projectSelectLocalidad: "",
              projectsList: [],
              estadosList: [],
              localidadesList: [],
              inventarioList: [],
            });

            alert("Inventario Asignado");
          }
        }
      } else {
        alert("Selecciona un Proyecto");
      }
    } else {
      alert("Selecciona un Usuario");
    }
  };

  /**
   *
   */
  selectProjectSearch = (e) => {
    console.log("Search -----");
  };

  /**
   * Selecciona el boton con la accion a realziar
   * @param {*} e
   */
  selectAction = (e) => {
    e.preventDefault();

    this.setState({
      userListSelect: 0,
      projectSelect: 0,
      projectSelectEstado: "",
      projectSelectLocalidad: "",
      projectsList: [],
      estadosList: [],
      localidadesList: [],
      inventarioList: [],
    });

    console.log(this.state.show_btn_assing);

    if (this.state.show_btn_assing === true) {
      this.setState({ show_btn_assing: false, describe: "Buscar" });
    } else {
      this.setState({ show_btn_assing: true, describe: "Asignar" });
    }
  };

  render() {
    return (
      <React.Fragment>
        <NavBar inHome={false} history={this.props.history} />

        <div id="asign-projects" className="manage-container">
          <div className="main-container">
            <div className="header-container">
              <div>
                <h3>Asignación</h3>
              </div>
            </div>

            <div className="container">
              <Row>
                <div className="btn-action">
                  <Button onClick={this.selectAction.bind(this)}>
                    {this.state.describe + " "}

                    {this.state.describe === "Buscar" ? (
                      <BsSearch />
                    ) : (
                        <BsFillPersonCheckFill />
                    )}

                  </Button>
                </div>

                <div className="btn-action">
                  <Button
                    onClick={this.asignInventario.bind(this)}
                    disabled={this.state.show_btn_assing}
                  >
                    Asignar
                  </Button>
                </div>
              </Row>

              <Row className="justify-content-center">
                <Col sm="6" className="">
                  <Row>
                    <Form.Label column sm="4">
                      Empleados
                    </Form.Label>
                    <Col sm={8}>
                      <Form.Control
                        as="select"
                        value={this.state.userListSelect || "0"}
                        onChange={this.selectUser.bind(this)}
                      >
                        <option value="0">Seleccionar Usuario..</option>
                        {this.state.usersList.map((item) => (
                          <option key={item.usuarioid} value={item.usuarioid}>
                            {item.nombrecompleto}
                          </option>
                        ))}
                      </Form.Control>
                    </Col>
                  </Row>
                </Col>
              </Row>

              <Row className="justify-content-start">
                {!this.state.show_btn_assing ? (
                  <>
                    <Col sm="4" className="">
                      <Form.Label>Proyecto</Form.Label>
                      <Form.Control
                        as="select"
                        value={this.state.projectSelect || "0"}
                        onChange={this.selectProject.bind(this)}
                      >
                        <option value="0">Seleccionar Proyecto..</option>
                        {this.state.projectsList.map((project, index) => (
                          <option key={project[0]} value={project[0]}>
                            {project[1]}
                          </option>
                        ))}
                      </Form.Control>
                    </Col>

                    <Col sm="4" className="">
                      <Form.Label>Estado</Form.Label>
                      <Form.Control
                        as="select"
                        value={this.state.projectSelectEstado || ""}
                        onChange={this.selectProjectEstado.bind(this)}
                      >
                        <option value="">Seleccionar Estado..</option>
                        {this.state.estadosList.map((item) => (
                          <option key={item[1]} value={item[1]}>
                            {item[1]}
                          </option>
                        ))}
                      </Form.Control>
                    </Col>

                    <Col sm="4" className="">
                      <Form.Label>Localidad</Form.Label>
                      <Form.Control
                        as="select"
                        value={this.state.projectSelectLocalidad || ""}
                        onChange={this.selectProjectLocalidad.bind(this)}
                      >
                        <option value="">Seleccionar Localidad..</option>
                        {this.state.localidadesList.map((item) => (
                          <option key={item[2]} value={item[2]}>
                            {item[2]}
                          </option>
                        ))}
                      </Form.Control>
                    </Col>
                  </>
                ) : null}
              </Row>

              <div
                className={
                  !this.state.show_btn_assing
                    ? `row content-table mode-search`
                    : `row content-table`
                }
              >
                <div className="col-sm-12">
                  <BootstrapTable
                    ref="tableinv"
                    id="tableinventarios"
                    striped={true}
                    data={this.state.inventarioList}
                    keyField="inventarioid"
                    version="4"
                    pagination
                    options={this.state.options}
                    selectRow={
                      !this.state.show_btn_assing ? this.state.selectRow : []
                    }
                  >
                    <TableHeaderColumn
                      dataField="inventarioid"
                      hidden
                      className="active-column-header"
                      columnClassName="active-column-data"
                    >
                      ID INVENTARIO
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="proyecto"
                      className="active-column-header"
                      filter={{ type: "TextFilter", placeholder: "PROYECTO" }}
                      columnClassName="active-column-data"
                    >
                      PROYECTO
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="folio"
                      className="active-column-header"
                      filter={{ type: "TextFilter", placeholder: "FOLIO" }}
                      columnClassName="active-column-data"
                    >
                      FOLIO
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      ref="refEstado"
                      dataField="estado"
                      filter={{
                        type: "TextFilter",
                        condition: !this.state.show_btn_assing ? "eq" : "like",
                        placeholder: "ESTADO",
                      }}
                      className="active-column-header"
                      columnClassName="active-column-data"
                    >
                      ESTADO
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      ref="refLocalidad"
                      dataField="localidad"
                      filter={{
                        type: "TextFilter",
                        condition: !this.state.show_btn_assing ? "eq" : "like",
                        placeholder: "LOCALIDAD",
                      }}
                      className="active-column-header"
                      columnClassName="active-column-data"
                    >
                      LOCALIDAD
                    </TableHeaderColumn>
                  </BootstrapTable>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Asignacion;
