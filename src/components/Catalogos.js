import XLSX from "xlsx";
import React from "react";
import "../assets/css/Catalogos.css";
import { Form, Col, Row, Button } from "react-bootstrap";
import { BsUpload } from "react-icons/bs";
import * as ApiServices from "../Services/ApiServices";
import { NavBar } from "./NavBar";

// SpreadJS imports
import "@grapecity/spread-sheets-react";
/* eslint-disable */
import "@grapecity/spread-sheets/styles/gc.spread.sheets.excel2016colorful.css";

class ModeloCatalogos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      catalogsModels: [],
      // Option to Table of Employs
      options: {
        sizePerPageList: [
          {
            text: "5",
            value: 5,
          },
          {
            text: "10",
            value: 10,
          },
          {
            text: "15",
            value: 15,
            //text: 'All', value: inventariosid.length
          },
        ], // you can change the dropdown list for size per page
        sizePerPage: 5, // which size per page you want to locate as default
        paginationPosition: "top", // default is bottom, top and both is all available
        noDataText: "Sin registros",
      },
      filteroptions: {
        1: 1,
        2: 2,
      },
    };
  }

  componentDidMount() {
    this.getCatalogsModel();
  }

  /**
   * Evento of Input File
   * @param {*} e
   */
  fileChangedHandler = async (e) => {
    e.preventDefault();

    if (e.target.files.length > 0) {
      var f = e.target.files[0];
      let dataCampos = new FormData();
      dataCampos.append("file", f);

      let result = await ApiServices.loadGeneralCatalogsFile(dataCampos);

      if (result) {
        this.refs.refCatalogo.cleanFiltered();
        this.refs.refDescription.cleanFiltered();
        this.refs.refModel.cleanFiltered();
        this.refs.refTipo.cleanFiltered();

        if (result.size > 0) {
          var bad = document.getElementById("inventario_bad");
          bad.href = URL.createObjectURL(result);
          bad.setAttribute("download", "Error_Catalogos.xlsx");
          bad.click();

          alert("Archivo presenta Errores!");
        } else {
          if (result.ok) {
            alert("Archivo cargado Correctamente!");
            this.getCatalogsModel();
          } else {
            alert("Error en la carga del Archivo!");
          }
        }
      }

      var inpf = this.refs.fileInput;
      inpf.value = "";
    } else {
      console.log("No File");
    }
  };

  /**
   * Even to open FileUpload
   * @param {*} e
   */
  clickUpload = (e) => {
    e.preventDefault();
    this.refs.fileInput.click();
  };

  /**
   *
   */
  getCatalogsModel = async () => {
    let result = await ApiServices.getCatModel();
    console.log(result);
    if (result !== null) this.setState({ catalogsModels: result });
  };

  render() {
    return (
      <>
        <NavBar inHome={false} history={this.props.history} />

        <div id="catalogs" className="manage-container">
          <input
            id="input-file"
            hidden
            type="file"
            type="file"
            name="upload_project"
            accept=".xlsx"
            onChange={this.fileChangedHandler.bind(this)}
            ref="fileInput"
          />

          <div className="main-container">
            <div className="header-container">
              <div>
                <h3>Catalogos</h3>
              </div>
            </div>

            <div className="container body-container">
              <Row>
                <Col>
                  <div className="btn-action">
                    <Button
                      onClick={this.clickUpload.bind(this)}
                      disabled={this.state.show_btn_alta}
                    >
                      Archivo <BsUpload />
                    </Button>
                  </div>
                </Col>
              </Row>

              <div className={"row content-table"}>
                <div className="col-sm-12">
                  <BootstrapTable
                    ref="tablecatalogs"
                    id="tablecatalogs"
                    className="tablecatalogs"
                    striped={true}
                    data={this.state.catalogsModels}
                    keyField="idmarcamodelo"
                    version="4"
                    pagination
                    options={this.state.options}
                  >
                    <TableHeaderColumn
                      dataField="idcatalogo"
                      hidden
                      className="active-column-header"
                      columnClassName="active-column-data"
                    >
                      ID CATALOGO
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="idmarcamodelo"
                      hidden
                      className="active-column-header"
                      columnClassName="active-column-data"
                    >
                      ID MODELO
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      ref="refCatalogo"
                      dataField="nombrecatalogo"
                      className="active-column-header"
                      filter={{ type: "TextFilter", placeholder: "CATALOGO" }}
                      columnClassName="active-column-data"
                    >
                      CATALOGO
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      ref="refDescription"
                      dataField="descripcion"
                      className="active-column-header"
                      filter={{
                        type: "TextFilter",
                        placeholder: "DESCRIPCIÓN/MARCA",
                      }}
                      columnClassName="active-column-data"
                    >
                      DESCRIPCIÓN/MARCA
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      ref="refModel"
                      dataField="modelo"
                      className="active-column-header"
                      filter={{ type: "TextFilter", placeholder: "MODELO" }}
                      columnClassName="active-column-data"
                    >
                      MODELO
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      ref="refTipo"
                      dataField="tipo"
                      className="active-column-header"
                      filter={{
                        type: "SelectFilter",
                        selectText: "",
                        options: this.state.filteroptions,
                      }}
                      columnClassName="active-column-data"
                    >
                      TIPO
                    </TableHeaderColumn>
                    <TableHeaderColumn
                      dataField="idmarca"
                      hidden
                      className="active-column-header"
                      columnClassName="active-column-data"
                    >
                      FOLIO
                    </TableHeaderColumn>
                  </BootstrapTable>
                </div>
              </div>
            </div>
          </div>
        </div>

        <a id="inventario_bad" className="hide">
          Descargar
        </a>
      </>
    );
  }
}

export default ModeloCatalogos;
