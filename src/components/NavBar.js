import React from "react";
import { Link } from "react-router-dom";
import auth from "./Login/auth";

export const NavBar = props => {

    const closeSession = (e) => {
        console.log("Close Session");
        auth.logout(() => {
            props.history.push("/");
        });
    }

return (
    <nav id="globalnav" className="nav-home">
        <div className="container-md justify-content-end">
        
        {!props.inHome ? 
            <Link className='home-option' to="/Home" >HOME</Link> 
        : null}

        <Link className="close-session" to="/" onClick={closeSession}>Cerrar Sesión</Link>

        {/*<div className="info-user">

            <div className="image"></div>

            <div>Nombre: </div>            
        </div>    */}
        </div>
    </nav>
  );
};

export default NavBar;


